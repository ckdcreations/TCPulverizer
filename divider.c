#include <stdlib.h>
#include <stdio.h>

#ifndef NUMERATOR
#define NUMERATOR 200
#endif

#ifndef DENOMINATOR
#define DENOMINATOR 1000
#endif

int main()
{
    double result;


    if(DENOMINATOR == 0)
    {
        perror("Can not divide by 0!");
        exit(1);
    
    }
    else
    {
        result = (double)NUMERATOR / (double)DENOMINATOR;
    }

    fprintf(stdout, "%f", result);


    return 0;
}
