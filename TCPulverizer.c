// Include Libraries
#include <dirent.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>

/******************************************************************************************************
 * Macros. IP_ADDRESS, PORT_NUM, and PROTOCOL 
 * Each have default values and can be adjusted through the compiler.
 *****************************************************************************************************/
#ifndef IP_ADDRESS
#define IP_ADDRESS                     "192.168.1.106"
#endif

#ifndef PORT_NUM
#define PORT_NUM                        48631
#endif

#ifndef PROTOCOL
#define PROTOCOL                        0
#endif


/******************************************************************************************************
 * sigintHandler function. Used to ensure that pressing Ctrl+C does not kill the program. Necessary
 * when used with the pulverize bash script so that erroneous errors are not registered.
 *****************************************************************************************************/
void sigintHandler(int sigNum)
{
    signal(SIGINT, sigintHandler);
}

/******************************************************************************************************
 * main function. Runs the program
 *****************************************************************************************************/
int main()
{
    // Stop a sigint signal from causing the running program to quit.
    signal(SIGINT, sigintHandler);

    // Variables to initialize. 
    int socketFD, msgLength, closeError, errsrv;
    ssize_t writeError, readError;
    struct sockaddr_in serverAddress;
    char *msg, rcvMsg;

    // The message to be sent to the other end of the socket. In this case, it is the Symetrix 'gs 1'
    // command. This command is used because it is the command continuously being used by Audio Brains.
    msg = "gs 1\r";
    msgLength = strlen(msg);

    // Set up the connection to make. 
    memset((char *)&serverAddress, '\0', sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(PORT_NUM);
    serverAddress.sin_addr.s_addr = inet_addr(IP_ADDRESS);

    
/******************************************************************************************************
 * Connect, send the message, receive the reply, then close the connection.
 *****************************************************************************************************/
       
    // Create the socket. AF_INET specifies normal internet based IP addresses.
    // SOCK_STREAM specifies a continuous stream requiring a TCP connection. 
    // SOCK_DGRAM would specify a UDP connection.
    if(PROTOCOL == 0)
    {
        if( (socketFD = socket(AF_INET, SOCK_STREAM, 0)) < 0)
        {
            perror("Error opening socket");
            exit(2);
        }
    }
    else if(PROTOCOL == 1)
    {
        if( (socketFD = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
        {
            perror("Error opening socket");
            exit(2);
        }
    }

    // Connect the socket to the server at the IP address
    if(connect(socketFD, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) < 0)
    {
        perror("Error connecting");
        exit(1);
    }
    // Assuming the connection is fine, send the message, receive the response, then close the socket.
    else 
    {
        writeError = write(socketFD, msg, msgLength);
        if(writeError < 0)
        {
            errsrv = errno;
            fprintf(stderr, "Write error with code: %d.\n", errsrv);
            exit(3);
        }
        else if(writeError >= 0 && writeError != msgLength)
        {
            perror("Write error: Incomplete message sent.");
            exit(3);
        }

        readError = read(socketFD, &rcvMsg, 5);
        if(readError < 0)
        {
            errsrv = errno;
            fprintf(stderr, "Read error with code: %d.\n", errsrv);
            exit(4);
        }
        if(close(socketFD) < 0)
        {
            errsrv = errno;
            fprintf(stderr, "Error closing socket with code: %d.\n", errsrv);
            exit(5);
        }
    };
   
    
    // End program.
    return 0;
}
