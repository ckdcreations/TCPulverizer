TCPulverizer

This is the Symetrix TCPulverizer! This software will open TCP connections to a Symetrix device on the network. 

To run, simply type ./pulverize [options]

Options:

-a IP_ADDRESS

-t time in ms

-p network port

-c transmission protocol (0 = TCP, 1 = UDP)

Example: ./pulverize -a 192.168.1.106 -t 200

This will run indefinitely until the user presses Ctrl+C. Once this is done, a new folder named 'results' will appear in the directory and a log of the results will be in this folder.

Enjoy!